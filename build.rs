extern crate bindgen;

use std::env;
use std::path::PathBuf;

fn main() {
    // Tell cargo to tell rustc to link the system bzip2
    // shared library.
    println!("cargo:rustc-link-lib=pipewire-0.2");

    // The bindgen::Builder is the main entry point
    // to bindgen, and lets you build up options for
    // the resulting bindings.
    let bindings = bindgen::Builder::default()
        // The input header we would like to generate
        // bindings for.
        .header("/usr/include/pipewire/pipewire.h")
        .header("/usr/include/spa/utils/list.h")
        .whitelist_type("^pw_.*")
        .whitelist_function("^pw_.*")
        .whitelist_type("^spa_.*")
        .whitelist_function("^spa_.*")
        .bitfield_enum("pw_direction")
        .bitfield_enum("pw_link_state")
        .bitfield_enum("pw_node_state")
        .bitfield_enum("pw_port_state")
        .bitfield_enum("pw_remote_state")
        .bitfield_enum("pw_stream_flags")
        .bitfield_enum("pw_stream_state")
        .bitfield_enum("spa_direction")
        .bitfield_enum("spa_io")
        .bitfield_enum("spa_log_level")
        .bitfield_enum("spa_monitor_item_flags")
        .bitfield_enum("spa_monitor_item_state")
        .bitfield_enum("spa_node_param_flags")
        .bitfield_enum("spa_pod_type")
        // .blacklist_type("max_align_t")
        // Finish the builder and generate the bindings.
        .generate()
        // Unwrap the Result and panic on failure.
        .expect("Unable to generate bindings");

    // Write the bindings to the $OUT_DIR/bindings.rs file.
    let out_path = PathBuf::from(env::var("OUT_DIR").unwrap());
    bindings
        .write_to_file(out_path.join("pipewire.rs"))
        .expect("Couldn't write bindings!");
}
